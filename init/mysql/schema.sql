-- --------------------------------------------------------
-- Host:                         pluto
-- Server version:               10.1.38-MariaDB-0+deb9u1 - Raspbian 9.0
-- Server OS:                    debian-linux-gnueabihf
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table euspider_v3.api_rss
CREATE TABLE IF NOT EXISTS `api_rss` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `lat` decimal(22,18) DEFAULT NULL,
  `lon` decimal(22,18) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.api_twitter
CREATE TABLE IF NOT EXISTS `api_twitter` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `twitter_list_id` varchar(50) DEFAULT NULL,
  `allowRTs` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.data_cities
CREATE TABLE IF NOT EXISTS `data_cities` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `asciiname` varchar(255) DEFAULT NULL,
  `alternatenames` text,
  `lat` decimal(12,8) DEFAULT NULL,
  `lon` decimal(12,8) DEFAULT NULL,
  `countrycode` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.data_countries
CREATE TABLE IF NOT EXISTS `data_countries` (
  `name` varchar(255) DEFAULT NULL,
  `capital` varchar(255) DEFAULT NULL,
  `lat` decimal(22,18) DEFAULT NULL,
  `lon` decimal(22,18) DEFAULT NULL,
  `countrycode` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.data_keywords
CREATE TABLE IF NOT EXISTS `data_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(100) NOT NULL DEFAULT '',
  `city` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `source_id` int(255) NOT NULL,
  `stub` varchar(14) NOT NULL,
  `dt` int(12) NOT NULL,
  `url` varchar(255) NOT NULL,
  `text` mediumtext NOT NULL,
  `entity` varchar(255) DEFAULT NULL,
  `lat` decimal(22,18) DEFAULT NULL,
  `lon` decimal(22,18) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `items_to_item_source` (`source_id`),
  CONSTRAINT `items_to_item_source` FOREIGN KEY (`source_id`) REFERENCES `item_sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table euspider_v3.item_sources
CREATE TABLE IF NOT EXISTS `item_sources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` varchar(16) NOT NULL,
  `pfp` varchar(255) NOT NULL,
  `lat` decimal(20,14) DEFAULT NULL,
  `lon` decimal(20,14) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
