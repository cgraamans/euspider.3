export default {
	
	// DATABASE SETTINGS
	db:{
		user:process.env.EUSPIDERV3_DB_USERNAME,
		password:process.env.EUSPIDERV3_DB_PASSWD,
		database:process.env.EUSPIDERV3_DB,
		host:'localhost',
		multipleStatements: true,
		charset:'utf8mb4'
	},

	// TWITTER SETTINGS
	twitter:{

		api: {
			consumer_key: process.env.EUSPIDER_TW_C_KEY,
			consumer_secret: process.env.EUSPIDER_TW_C_SECRET,
			access_token_key: process.env.EUSPIDER_TW_AT_KEY,
			access_token_secret: process.env.EUSPIDER_TW_AT_SECRET
		},

		// Maximum number of total hits to twitter api
		// Note: 0 defaults to 10s
		// Note: 900 p/15 minutes = 60 hits per minute max , reduce by 5 for lag
		max_hits:0 // hpm

	},

	// LOCATION WORKER SETTINGS
	locations:{

		// interval in ms
		interval: 10000
	
	}

};