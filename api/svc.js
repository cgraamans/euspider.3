'use strict';

import App from './lib/app.js';
import options from './options.js';

let app = new App(options);

try {

	app.io.on('connection',socket=>{

		// console.log(JSON.stringify(socket.handshake))
		// console.log(socket.id);
		console.log('connection!');

		socket.on("*",packet=>{

			// console.log(packet);

			if(packet.data && packet.data.length === 2) {

				let event = packet.data[0],
					data = packet.data[1];

				if(event && data) {

					app.log(`${event} received`);

					console.log(data);

					let activeRoute = app.route(event);

					if(activeRoute) {

						activeRoute(data)
							.then(()=>{

								app.log(`${event} routed and executed`);

							})
							.catch(e=>{

								throw new Error(e)

							});
					}

				}

			}

		});

		// socket.on('send',data=>{

		// 	console.log('send',data);

		// })

		// setInterval(function(){

		// 	socket.emit('socket-news',{data:'news'});
		// 	// io.emit('io-news',{data:'news'});
		// 	socket.to('game1').to('game2').emit('game-news',{data:'news'});

		// },5000)

		socket.on('disconnect',data=>{
			console.log('disconnected',data);
			console.log(socket.handshake);
		});

	});

} catch(e) {

	app.log(e,'E');

}