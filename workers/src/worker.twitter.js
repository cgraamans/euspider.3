import twitter from 'twitter';
import APP from '../lib/app.js';
import options from '../options.js';
import shortid from 'shortid';

// prepare class variables
let TwitterClient = new twitter(options.twitter.api);
let app = new APP(options);

let intervals = [];

// set timers
let startTimeLength = 60000;
let timerLength = 30000;
//
// Functions
//

// stop intervals
let stopIntervals = ()=>{

	intervals.forEach(interval=>{
		clearInterval(interval);
	});	

};

// fetch tweets
let fetchTweets = listId=>{

	return new Promise((resolve,reject)=>{

		TwitterClient.get("lists/statuses", {list_id:listId,tweet_mode:'extended'}, function(e, statuses, r) {	

			if(e) app.log(e);

			let rtn = [];
			if(statuses && statuses.length>0) rtn = statuses;
			resolve(rtn);
		
		});

	});

};

// filter sources
let filterSources = tweets => {

	return new Promise((resolve,reject)=>{

		let promiseList = [];
		let tweetList = [];

		// reduce
		tweets.forEach(tweet=>{

			tweetList.push({
				name:tweet.user.screen_name,
				pfp:tweet.user.profile_image_url_https
			});

		});

		// Filter by unique name value
		let uniques = Array.from(new Set(tweetList.map(p => p.name)))
						.map(name=>{
							return {
								name:name,
								pfp:tweetList.find(t=>t.name === name).pfp
							};
						});


		// Check the database for duplicates
		uniques.forEach(unique=>{
			
			if(unique.pfp.includes("_normal.")) unique.pfp = unique.pfp.replace('_normal.','.');

			promiseList.push(new Promise((res,rej)=>{

				app.q("SELECT pfp FROM item_sources WHERE BINARY name = ?",[unique.name])
					.then(sourceCheck=>{

						if(sourceCheck.length < 1) {

							res(unique);

						} else {

							app.q("UPDATE item_sources SET pfp = ? WHERE name = ?",[unique.pfp,unique.name])
								.then(updResult=>{

									if(updResult.affectedRows>0) {

										res();

									} else {

										app.log(`No changes made to ${unique.name}'s profile picture`);
										res();

									}

								}).catch(e=>{

									rej(e);

								});;

						}

					})
					.catch(rej);

			}));

		});

		Promise.all(promiseList)
			.then(list=>{
				list = list.filter(Boolean);
				resolve(list);
			})
			.catch(reject);

	});

};

let filterItems = tweets => {

	return new Promise((resolve,reject)=>{

		let promiseList = [];

		tweets.forEach(tweet=>{

			promiseList.push(new Promise((res,rej)=>{

				let dt = new Date(tweet.created_at).getTime()/1000;
				if(!dt || typeof dt !== 'number') dt = new Date().getTime()/1000;

				app.q("SELECT id FROM items WHERE BINARY text = ? AND dt = ?",[tweet.full_text,dt])
					.then(duplicateCheck=>{

						if(duplicateCheck.length < 1) {

							app.q("SELECT id FROM item_sources WHERE name = ?",[tweet.user.screen_name])
								.then(idArr=>{

									if(idArr.length > 0) {

										let filteredItem = {
											dt: dt,
											stub:shortid.generate(),
											source_id:idArr[0].id,
											text:tweet.full_text,
											url:"https://twitter.com/"+tweet.user.screen_name+"/status/"+tweet.id_str
										};

										if(tweet.entities && tweet.entities.media && tweet.entities.media.length > 0) filteredItem.entity = tweet.entities.media[0].media_url_https;

										res(filteredItem);

									} else {

										rej(`MISSING ITEM SOURCE FOR ${tweet.user.screen_name}`);
									
									}
								})
								.catch(e=>rej(e));

						} else {

							res();

						}

					})
					.catch(e=>rej(e))

			}));

		});

		Promise.all(promiseList)
			.then(list => {
				resolve(list.filter(Boolean));
			})
			.catch(e => reject(e));

	});

};

let setItems = items => {

	return new Promise((resolve,reject)=>{

		let promiseList = [];

		items.forEach(item=>{

			console.log(item);

			promiseList.push(new Promise((res,rej)=>{

				app.q("INSERT INTO items SET ?",item)
					.then(res)
					.catch(rej);

			}));

		});

		Promise.all(promiseList)
			.then(()=>resolve(items))
			.catch(e=>reject(e));

	});

};

// push to sources table
let setSources = sources => {

	return new Promise((resolve,reject)=>{

		let promiseList = [];

		sources.forEach(source=>{

			source = Object.assign(source,{type:'twitter'});

			promiseList.push(new Promise((res,rej)=>{

				app.q("INSERT INTO item_sources SET ?",source)
					.then(res)
					.catch(rej);

			}));

		});

		Promise.all(promiseList)
			.then(()=>resolve(sources))
			.catch(e=>reject(e));

	});

};

// transform tweets for item_sources and items
let procTweets = tweets=>{

	return new Promise((resolve,reject)=>{

		let rtn = {
			sources:0,
			items:0
		};

		filterSources(tweets)
			.then(filteredSources=>{

				return setSources(filteredSources);
			
			})
			.then(sources=>{

				rtn.sources = sources.length;
				return rtn;

			})
			.then(()=>{
				return filterItems(tweets);
			})
			.then(items=>{
				return setItems(items);
			})
			// .then(setItems)
			.then(items=>{
				// console.log('items');
				// console.log(items);
				rtn.items = items.length;
				return;
			})
			.then(()=>{
				resolve(rtn)
			})
			.catch(reject);

	});

};

//
// Initializing function
//

let init = async ()=>{
	
	try {

		// get all twitter lists to watch
		const TwitterList = await app.q('SELECT * from api_twitter',[])

		if(options.twitter.max_hits > 0) timerLength = Math.floor((60/Math.floor((options.twitter.max_hits/TwitterList.length))) * 1000);
		
		TwitterList.forEach(listItem=>{

			let startTimeout = Math.floor(Math.random()*startTimeLength);
			app.log(`${listItem.name} interval starts in ${startTimeout+timerLength}ms`);

			setTimeout(()=>{

				intervals.push(setInterval(async ()=>{

					// fetch tweets
					let tweets = await fetchTweets(listItem.twitter_list_id).catch(e=>{throw e});
					
					// process tweets
					let procResult = await procTweets(tweets).catch(e=>{throw e});

					app.log(`${listItem.name}: ${procResult.sources} sources, ${procResult.items} items`);

				},timerLength));

			},startTimeout);

		});

	} catch(e) {

		console.log('FATAL ERROR');
		console.log(e);
		stopIntervals();

	}

};

init();