import APP from '../lib/app.js';

import io from 'socket.io-client'
import options from '../options.js';
import shortid from 'shortid';

// prepare class variables
let app = new APP(options);

let intervals = [];

// set timers
let startTimeLength = 60000;
let timerLength = 30000;
//
// Functions
//

// stop intervals
let stopIntervals = ()=>{

	intervals.forEach(interval=>{
		clearInterval(interval);
	});	

};

// process cities table
let cities = item => {

	return new Promise((resolve,reject)=>{

		try {

			resolve(item);

		} catch(e) {

			reject(e);

		}

	});

};

// process countries table
let countries = item => {

	return new Promise((resolve,reject)=>{

		try {

			resolve(item);

		} catch(e) {

			reject(e);

		}

	});

};

// process keywords table
let keywords = item => {

	return new Promise((resolve,reject)=>{

		try {

			resolve(item);

		} catch(e) {

			reject(e);

		}

	});

};


let insertItemLocations = items => {

	return new Promise((resolve,reject)=>{

		try {

			resolve(items);

		} catch(e) {

			reject(e);

		}

	});	

};

// run through all data tables
let Proc = items=>{

	return new Promise((resolve,reject)=>{

		try {

			let itemPromiseList = [];

			items.forEach(item=>{

				itemPromiseList.push(new Promise((res,rej)=>{

					let functionList = [cities(item),countries(item),keywords(item)]
					Promise.all(functionList)
						.then(ProcessedItem=>{

							res(ProcessedItem);

						})
						.catch(e=>{

							throw e;

						});

				}));

			});

			Promise.all(itemPromiseList)
				.then(itemList=>{

					insertItemLocations(itemList)
						.then(()=>{

							resolve(itemList);

						})
						.catch(e=>{

							throw e;

						});

				})
				.catch(e=>{

					throw e;

				});

		} catch(e) {

			reject(e);

		}


	});

};

// initialize interval(s)
let intervalInit = () => {

	try {

		return setInterval(()=>{

			app.q(`
					SELECT * FROM items AS i 
					INNER JOIN item_sources isource ON isource.id = i.source_id 
					WHERE lat IS NULL 
					AND lon IS NULL
					ORDER BY dt ASC
					LIMIT 10
				`,
				[])
				.then(items=>{

					Proc(items)
						.then(items=>{

							app.log(`Location - Items processed: ${items.length}`);

						})
						.catch(e=>{

							throw e;

						});

				});

		},options.locations.interval);

	} catch(e){

		throw new Error(e);

	}

}

//
// Initializing function
//

let init = ()=>{
	
	try {

		intervals.push(intervalInit());

	} catch(e) {

		console.log('FATAL ERROR');
		console.log(e);
		stopIntervals();

	}

};

init();