export default class App {

	constructor(options){

		this.io = require('socket.io')(options.io.port);
		this.argv = require('minimist')(process.argv.slice(2))

		let mysql = require('mysql');
		this.pool = mysql.createPool(options.db);

		// activate REDIS
		if(this.argv.redis && options.io.redis){
		
			let redisAdapter = require('socket.io-redis');
			this.io.adapter(redisAdapter(options.io.redis));
		
		}
		
		// activate wildcard
		let wildcard = require('socketio-wildcard')();
		this.io.use(wildcard);
	
		let defaultRoutes = require('./defaultRoutes.js');
		this.routes = Object.assign({},options.routes,defaultRoutes);

		// console colors
		this.colors = require('colors');

	}

	// Execute MySQL query
	q(s,v) {

		var that = this;
		return new Promise((resolve,reject)=> {

			try {

				that.pool.getConnection(function(err, connection) {
				  if (err) throw err;

				  connection.query(s,v,(error, results, fields)=>{

				    connection.release();

				    if (error) throw error;
				    resolve(results);

				  });

				});

			} catch(e) {

				reject(e);

			}

		});

	}

	log(e,lvl) {
		if(!lvl) lvl = 'W'
		if(!this.argv.prod) console.log(e); 
	}

	route(r) {

		if(this.routes.hasOwnProperty(r)) {

			let rtn = require(this.routes[r].src);
			if(rtn) return rtn;
			return;
		
		}

	}

}