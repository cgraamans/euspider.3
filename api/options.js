export default {
	
	// DATABASE SETTINGS
	db:{
		user:process.env.EUSUBSPIDER_DB_USERNAME,
		password:process.env.EUSUBSPIDER_DB_PASSWD,
		database:process.env.EUSUBSPIDER_DB,
		host:'localhost',
		multipleStatements: true,
		charset:'utf8mb4'
	},
	
	// SOCKET.IO PORT SETTINGS
	io:{
		port:9001,
		
		// REDIS settings
		// Uncomment

		// redis:{ 
		// 	host: 'localhost', 
		//	port: 6379 
		// }

	},

	// Routes
	routes:{

		'items.get':{
			src:'./src/items.get.js',
		}
		// 'test': {
		// src: 'test.js'
		// }

	}

};