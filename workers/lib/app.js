export default class APP {

	constructor(options){

		try {

			this.argv = require('minimist')(process.argv.slice(2))
			let mysql = require('mysql');
			this.pool = mysql.createPool(options.db);

		} catch(e) {

			throw e;

		}

	}

	// Execute MySQL query
	q(s,v) {

		var that = this;
		return new Promise((resolve,reject)=> {

			try {

				that.pool.getConnection(function(err, connection) {
				  if (err) throw err;

				  connection.query(s,v,(error, results, fields)=>{

				    connection.release();
				    if (error) throw error;
				    resolve(results);

				  });

				});

			} catch(e) {

				reject(e);

			}

		});

	}

	log(e) {
		if(!this.argv.prod) console.log(e); 
	}

}